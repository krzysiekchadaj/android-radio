# [Android] Radio
An Android app for radio streaming.

![Radio screen](img/screen1.jpg)
![Radio screen](img/screen2.jpg)

## Disclaimer
You can use the software for educational purposes only. The information
regarding the origin of the software shall be preserved.

The software is provided "as is", and without warranty of any kind. In 
no event shall the author be liable for any claim, tort, damage, or any
other liability.

By using the program, you agree to the above terms and conditions.
