package com.kchadaj.radio.logic;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.RestrictTo;
import android.util.Log;

import com.kchadaj.radio.data.Station;

import java.util.concurrent.ExecutionException;

import static com.kchadaj.radio.logic.RadioNotification.ACTION_PAUSE;
import static com.kchadaj.radio.logic.RadioNotification.ACTION_PLAY;
import static com.kchadaj.radio.logic.RadioNotification.ACTION_STOP;

public class RadioService extends Service implements MediaPlayer.OnErrorListener {
    private static final String TAG = RadioService.class.getSimpleName();

    static final String INTENT_STATION = "Station";

    PlayerThread playerThread;
    Station station;
    RadioBroadcastReceiver receiver;

    private RadioNotification radioNotification;
    private MediaPlayer player;
    private Boolean isPlaying;

    @Override
    public void onCreate() {
        radioNotification = new RadioNotification(this);
        startForeground(RadioNotification.NOTIFICATION_ID, radioNotification.get());
        initializeReceiver();
    }

    private void initializeReceiver() {
        receiver = new RadioBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_PLAY);
        filter.addAction(ACTION_PAUSE);
        filter.addAction(ACTION_STOP);
        registerReceiver(receiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        station = intent.getParcelableExtra(INTENT_STATION);
        startPlayer();
        updateNotification();
        return START_STICKY;
    }

    private void startPlayer() {
        if (player != null && player.isPlaying()) {
            player.stop();
            player.release();
        }

        player = new MediaPlayer();
        player.setOnErrorListener(this);

        try {
            playerThread = new PlayerThread(player, station);
            playerThread.execute();
            playerThread.get();
        } catch (InterruptedException | ExecutionException e) {
            isPlaying = false;
            Log.d(TAG, "Unable to play radio station", e);
        }
        isPlaying = true;
    }

    private void updateNotification() {
        if (radioNotification.isChannelEnabled()) {
            radioNotification.update(station, isPlaying);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "Player error occurred: " + what + " " + extra);
        return false;
    }

    class RadioBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ACTION_PAUSE:
                        pause();
                        break;

                    case ACTION_PLAY:
                        play();
                        break;

                    case ACTION_STOP:
                        stop();
                        break;
                }
            }
        }

        private void pause() {
            if (player.isPlaying()) {
                player.pause();
                isPlaying = false;
                updateNotification();
            }
        }

        private void play() {
            if (!player.isPlaying()) {
                player.start();
                isPlaying = true;
                updateNotification();
            }
        }

        private void stop() {
            unregisterReceiver(receiver);
            playerThread.cancel(true);
            player.stop();
            player.reset();
            player.release();
            radioNotification.cancel();
            stopSelf();
        }
    }
}
