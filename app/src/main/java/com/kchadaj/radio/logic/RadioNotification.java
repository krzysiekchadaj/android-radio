package com.kchadaj.radio.logic;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.app.NotificationCompat.MediaStyle;

import com.kchadaj.radio.R;
import com.kchadaj.radio.data.Station;
import com.kchadaj.radio.view.RadioActivity;

class RadioNotification {
    static final int NOTIFICATION_ID = 42;
    private static final int REQUEST_CODE = 4242;
    private static final String CHANNEL_ID = RadioNotification.class.getName() + ".channel";

    static final String ACTION_PLAY = "ACTION_PLAY";
    static final String ACTION_PAUSE = "ACTION_PAUSE";
    static final String ACTION_STOP = "ACTION_STOP";

    private final NotificationCompat.Action playAction;
    private final NotificationCompat.Action pauseAction;
    private final NotificationCompat.Action stopAction;

    private final Context context;
    private final NotificationManager manager;

    RadioNotification(Context context) {
        this.context = context;

        this.manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        playAction = defineAction(R.drawable.ic_play, R.string.action_play, ACTION_PLAY);
        pauseAction = defineAction(R.drawable.ic_pause, R.string.action_pause, ACTION_PAUSE);
        stopAction = defineAction(R.drawable.ic_stop, R.string.action_stop, ACTION_STOP);
    }

    private NotificationCompat.Action defineAction(int drawableId, int titleStringId, String action) {
        return new NotificationCompat.Action(
                drawableId,
                context.getString(titleStringId),
                PendingIntent.getBroadcast(context, REQUEST_CODE, new Intent(action), 0)
        );
    }

    Notification get() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
        return builder(new Station(), false).build();
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel() {
        if (manager.getNotificationChannel(CHANNEL_ID) == null) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, context.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(context.getString(R.string.description_channel));
            manager.createNotificationChannel(mChannel);
        }
    }

    void update(Station station, Boolean isPlaying) {
        NotificationCompat.Builder mNotifyBuilder = builder(station, isPlaying);
        manager.notify(NOTIFICATION_ID, mNotifyBuilder.build());
    }

    private NotificationCompat.Builder builder(Station station, Boolean isPlaying) {
        return new NotificationCompat.Builder(context, CHANNEL_ID)
                .setStyle(new MediaStyle()
                        .setShowActionsInCompactView(0, 1)
                        .setShowCancelButton(true)
                        .setCancelButtonIntent(createActionIntent(ACTION_STOP))
                )
                .setSmallIcon(R.drawable.ic_radio)
                .setContentIntent(createContentIntent())
                .setContentTitle(station.getTitle())
                .setContentText(station.getFrequency())
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), station.getImageId()))
                .setDeleteIntent(createActionIntent(ACTION_STOP))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .addAction(isPlaying ? pauseAction : playAction)
                .addAction(stopAction);
    }

    private PendingIntent createContentIntent() {
        Intent openUI = new Intent(context, RadioActivity.class);
        openUI.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(context, REQUEST_CODE, openUI, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private PendingIntent createActionIntent(String action) {
        return PendingIntent.getBroadcast(context, REQUEST_CODE, new Intent(action), 0);
    }

    boolean isChannelEnabled() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = manager.getNotificationChannel(CHANNEL_ID);
            return channel.getImportance() != NotificationManager.IMPORTANCE_NONE;
        } else {
            return manager.areNotificationsEnabled();
        }
    }

    void cancel() {
        manager.cancel(NOTIFICATION_ID);
    }
}
