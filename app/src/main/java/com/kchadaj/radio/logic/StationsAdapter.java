package com.kchadaj.radio.logic;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kchadaj.radio.R;
import com.kchadaj.radio.data.Station;
import com.kchadaj.radio.data.StationsLibrary;
import com.kchadaj.radio.view.StationViewHolder;

import java.util.ArrayList;

public class StationsAdapter extends RecyclerView.Adapter<StationViewHolder> {

    private ArrayList<Station> stations = new ArrayList<>();

    public StationsAdapter() {
        stations = StationsLibrary.getInstance().getStationsList();
    }

    @Override
    public StationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View stationCard = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_station, parent, false);
        return new StationViewHolder(stationCard);
    }

    @Override
    public void onBindViewHolder(StationViewHolder holder, final int position) {
        final Station station = stations.get(position);

        holder.updateUI(station);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                if (isNetworkAvailable(context)) {
                    updateRadioService(context, station);
                } else {
                    Toast.makeText(context, R.string.error_network, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            return (null != networkInfo) && networkInfo.isConnected();
        }
        return false;
    }

    private void updateRadioService(Context context, Station station) {
        Intent radioServiceIntent = new Intent(context, RadioService.class);
        radioServiceIntent.putExtra(RadioService.INTENT_STATION, station);
        ContextCompat.startForegroundService(context, radioServiceIntent);
    }
}
