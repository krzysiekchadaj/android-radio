package com.kchadaj.radio.logic;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

import com.kchadaj.radio.data.Station;

import java.io.IOException;

class PlayerThread extends AsyncTask<Void, Void, Void> {
    private final String TAG = PlayerThread.class.getSimpleName();

    private MediaPlayer player;
    private Station station;

    PlayerThread(MediaPlayer player, Station station) {
        this.player = player;
        this.station = station;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            player.setDataSource(station.getUrl());
            player.prepare();
            player.start();
        } catch (IOException e) {
            Log.d(TAG, "Unable to play radio station", e);
        }
        return null;
    }
}
