package com.kchadaj.radio.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kchadaj.radio.R;
import com.kchadaj.radio.logic.StationsAdapter;

public class StationsFragment extends Fragment {

    public StationsFragment() {
        // empty
    }

    public static StationsFragment newInstance() {
        return new StationsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stations, container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerView recyclerView = v.findViewById(R.id.recycler_stations);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        StationsAdapter adapter = new StationsAdapter();
        recyclerView.setAdapter(adapter);

        return v;
    }
}
