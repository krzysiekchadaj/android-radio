package com.kchadaj.radio.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.kchadaj.radio.R;

public class RadioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        RadioFragment radioFragment = (RadioFragment) manager.findFragmentById(R.id.container_main);
        if (radioFragment == null) {
            radioFragment = RadioFragment.newInstance();
            manager.beginTransaction()
                    .add(R.id.container_main, radioFragment)
                    .commit();
        }
    }
}
