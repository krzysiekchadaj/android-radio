package com.kchadaj.radio.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kchadaj.radio.R;

public class RadioFragment extends Fragment {

    public RadioFragment() {
        // empty
    }

    public static RadioFragment newInstance() {
        return new RadioFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container_stations_featured, StationsFragment.newInstance())
                .commit();
        return v;
    }
}
