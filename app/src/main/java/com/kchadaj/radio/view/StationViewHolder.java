package com.kchadaj.radio.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kchadaj.radio.R;
import com.kchadaj.radio.data.Station;

public class StationViewHolder extends RecyclerView.ViewHolder {

    private ImageView mainImage;
    private TextView titleTextView;

    public StationViewHolder(View itemView) {
        super(itemView);

        this.mainImage = itemView.findViewById(R.id.main_image);
        this.titleTextView = itemView.findViewById(R.id.main_text);
    }

    public void updateUI(Station station) {
        mainImage.setImageResource(station.getImageId());
        titleTextView.setText(station.getTitle());
    }
}
