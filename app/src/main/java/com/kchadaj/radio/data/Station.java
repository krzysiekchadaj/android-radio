package com.kchadaj.radio.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Station implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Object createFromParcel(Parcel source) {
            return new Station(source);
        }

        @Override
        public Object[] newArray(int size) {
            return new Station[size];
        }
    };

    private String title;
    private int imageId;
    private String url;
    private String frequency;

    public Station() {
        this.title = "Radio";
        this.imageId = 0;
        this.url = "";
        this.frequency = "0";
    }

    Station(String Title, int imageId, String url, String frequency) {
        this.title = Title;
        this.imageId = imageId;
        this.url = url;
        this.frequency = frequency;
    }

    private Station(Parcel in) {
        this.title = in.readString();
        this.imageId = in.readInt();
        this.url = in.readString();
        this.frequency = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public int getImageId() {
        return imageId;
    }

    public String getUrl() {
        return url;
    }

    public String getFrequency() {
        return frequency + " MHz";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(imageId);
        dest.writeString(url);
        dest.writeString(frequency);
    }
}
