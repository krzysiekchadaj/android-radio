package com.kchadaj.radio.data;

import com.kchadaj.radio.R;

import java.util.ArrayList;

public final class StationsLibrary {

    private static StationsLibrary instance;

    private ArrayList<Station> stations;

    private StationsLibrary() {
        stations = new ArrayList<>();

        stations.add(new Station("Antyradio", R.drawable.station1, "http://ant-waw-01.cdn.eurozet.pl:8602/", "106.8"));
        stations.add(new Station("RMF FM", R.drawable.station2, "http://31.192.216.5:8000/rmf_fm", "90.6"));
        stations.add(new Station("Radio Zet", R.drawable.station3, "http://radiozetmp3-01.eurozet.pl:8400/", "107.5"));

        stations.add(new Station("Polskie Radio 1", R.drawable.station4, "http://stream3.polskieradio.pl:8900/", "92.4"));
        stations.add(new Station("Polskie Radio 2", R.drawable.station5, "http://stream3.polskieradio.pl:8902/", "104.9"));
        stations.add(new Station("Polskie Radio 3", R.drawable.station6, "http://stream3.polskieradio.pl:8904/", "99.1"));
    }

    public static StationsLibrary getInstance() {
        if (instance == null)
            instance = new StationsLibrary();
        return instance;
    }

    public ArrayList<Station> getStationsList() {
        return stations;
    }
}
