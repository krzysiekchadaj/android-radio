package com.kchadaj.radio;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;

import com.kchadaj.radio.data.Station;
import com.kchadaj.radio.data.StationsLibrary;
import com.kchadaj.radio.view.RadioActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeoutException;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class NotificationTest {

    private static final int TIMEOUT = 1000;

    private List<Station> stationsList;
    private UiDevice device;

    @Rule
    public ActivityTestRule<RadioActivity> radioActivity =
            new ActivityTestRule<>(RadioActivity.class);

    @Rule
    public ServiceTestRule radioService = new ServiceTestRule();


    @Before
    public void setup() throws TimeoutException {
        stationsList = StationsLibrary.getInstance().getStationsList();
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }


    @Test
    public void radioNotification_isDisplayed() {
        onView(withId(R.id.recycler_stations))
                .perform(scrollToPosition(0))
                .perform(actionOnItemAtPosition(0, click()));

        String appName = InstrumentationRegistry.getTargetContext().getResources().getString(R.string.app_name);
        device.openNotification();
        device.wait(Until.hasObject(By.text(appName)), TIMEOUT);

        String stationTitle = stationsList.get(0).getTitle();
        UiObject2 title = device.findObject(By.text(stationTitle));
        assertEquals(title.getText(), stationTitle);

        device.pressHome();
    }
}
