package com.kchadaj.radio;

import android.support.annotation.NonNull;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kchadaj.radio.data.Station;
import com.kchadaj.radio.data.StationsLibrary;
import com.kchadaj.radio.view.RadioActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.core.internal.deps.dagger.internal.Preconditions.checkNotNull;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class StationsListViewTest {

    private List<Station> stationsList;

    @Rule
    public ActivityTestRule<RadioActivity> radioActivity =
            new ActivityTestRule<>(RadioActivity.class);

    @Before
    public void setup() {
        stationsList = StationsLibrary.getInstance().getStationsList();
    }

    @Test
    public void stationsList_isDisplayed() {
        for (int i = 0; i < stationsList.size(); i++) {
            onView(withId(R.id.recycler_stations))
                    .perform(scrollToPosition(i))
                    .check(matches(atPosition(i, hasDescendant(withText(stationsList.get(i).getTitle())))));
        }
    }

    private Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        checkNotNull(itemMatcher);
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {

            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
                return viewHolder != null && itemMatcher.matches(viewHolder.itemView);
            }
        };
    }
}
