package com.kchadaj.radio.logic;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;

import com.kchadaj.radio.R;
import com.kchadaj.radio.view.RadioActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowApplication;

import static org.junit.Assert.assertEquals;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class StationsAdapterTest {

    @Test
    public void clickingStationItem_shouldSendServiceIntent() {
        ActivityController<RadioActivity> activityController = Robolectric.buildActivity(RadioActivity.class);
        activityController.create().start().visible();

        ShadowActivity shadowActivity = shadowOf(activityController.get());

        RecyclerView stationsRecyclerView = ((RecyclerView) shadowActivity.findViewById(R.id.recycler_stations));
        stationsRecyclerView.getChildAt(0).performClick();

        Intent expectedIntent = new Intent(stationsRecyclerView.getChildAt(0).getContext(), RadioService.class);
        Intent actualIntent = ShadowApplication.getInstance().getNextStartedService();
        assertEquals(expectedIntent.getComponent(), actualIntent.getComponent());
    }
}
