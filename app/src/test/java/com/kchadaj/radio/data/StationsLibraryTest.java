package com.kchadaj.radio.data;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.*;

public class StationsLibraryTest {

    private static final int TIMEOUT_MS = 1000;

    private List<Station> stationsList;

    @Before
    public void setup() {
        stationsList = StationsLibrary.getInstance().getStationsList();
    }

    @Test
    public void stationsList_isNotNullNorEmpty() {
        assertNotNull(stationsList);
        assertFalse(stationsList.isEmpty());
    }

    @Test
    public void stationList_hasValidStations() {
        for (Station station : stationsList) {
            assertTrue(isValidStation(station));
        }
    }

    private boolean isValidStation(Station station) {
        assertNotNull(station.getTitle());
        assertFalse(station.getTitle().isEmpty());
        assertNotEquals(station.getImageId(), 0);
        assertTrue(isValidStreamUrl(station.getUrl()));
        return true;
    }

    private boolean isValidStreamUrl(String streamURL) {
        try (Socket socket = new Socket()) {
            URL url = new URL(streamURL);
            InetSocketAddress address = new InetSocketAddress(url.getHost(), url.getPort());
            socket.connect(address, TIMEOUT_MS);
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
